const {BrowserWindow} = require('electron')

const newWindowBtn = document.getElementById('new-window')

newWindowBtn.addEventListener('click', (event) => {
  let win = new BrowserWindow({ width: 400, height: 320 })

  win.loadFile('page1.html')
  win.on('close', () => { win = null })
  
  win.show()
})