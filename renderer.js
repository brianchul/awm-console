// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.


const {BrowserWindow} = require('electron').remote

const newWindowBtn = document.getElementById('new-window')

newWindowBtn.addEventListener('click', (event) => {
  let win = new BrowserWindow({ width: 400, height: 320 })
  win.loadFile('page1.html')
  win.on('close', () => { win = null })
  
  win.show()
})

const redirNewPage = document.getElementById('washbtn')

redirNewPage.onclick = function(){
  console.log('run here')
  window.location.href='page1.html'
}